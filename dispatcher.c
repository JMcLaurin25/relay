/*
 * Dispatcher
 * - Resources -
 * http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html
 * http://www.binarytides.com/multiple-socket-connections-fdset-select-linux/
 * http://stackoverflow.com/questions/4200172/c-socket-programming-connecting-multiple-clients-to-server-using-select
 * http://www.tutorialspoint.com/unix_sockets/socket_core_functions.htm
 */

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "handler.h"

#define MAXBUFFER 1024
#define MAXCLIENT 10
#define DSPPORT 2468

char *msgBuf;

int main(void)
{
	int disp_id = 0, client_sd = 0, max_sd = 0;
	int conn_len = 0, msg_sz = 0;

	fd_set root; // Set of socket descriptors
	fd_set read_fds;
	FD_ZERO(&root); // Zero the socket descriptors
	FD_ZERO(&read_fds);
	 
	msgBuf = malloc(MAXBUFFER);
	if (!msgBuf) {
		printf("Failed to create message buffer\n");
		return 1;
	}
	memset(msgBuf, 0, MAXBUFFER);

	sig_init();

	//------------------------------------------------------------------------
	// Create socket
	struct sockaddr_in disp_addr;
	memset(&disp_addr, 0, sizeof(disp_addr));

	if ((disp_id = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Failed to create socket\n");
		free(msgBuf);
		return 1;
	}

	disp_addr.sin_family = AF_INET; // Set the socket family
	disp_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	disp_addr.sin_port = htons(DSPPORT); // Port to utilize

	// Bind address and port
	bind(disp_id, &disp_addr, sizeof(disp_addr));
	if (listen(disp_id, MAXCLIENT) == -1) {
		printf("Error listening for connections\n");
		free(msgBuf);
		return -1;
	}

	FD_SET(disp_id, &root); // Add socket to set
	FD_SET(STDIN_FILENO, &root);

	max_sd = disp_id;

	//------------------------------------------------------------------------
	conn_len = sizeof(disp_addr);
	// Connection loop
	printf("Dispatcher listening ...\n");

	while (1) {
		read_fds = root;

		// Allow selection of multiple file descriptors 
		if (select (max_sd + 1, &read_fds, NULL, NULL, NULL) == -1) {
			printf("Socket select error\n");
		}

		// Communication process
		for (int idx = 0; idx < MAXCLIENT; idx++) {

			// If connection established
			if (FD_ISSET(idx, &read_fds)) {
				if (idx == disp_id) {
					// Connect to new listener
					if ((client_sd = accept(
									disp_id,
								   	(struct sockaddr *)&disp_addr,
								   	(socklen_t *)&conn_len
									)
								) < 0) {
						printf("Client socket error\n");
					} else {
						// New listener connected to dispatcher
						const char *list_con = "Connected to dispatch...\0";
						send(client_sd, list_con, strlen(list_con) + 1, 0);

						FD_SET(client_sd, &root); // Add to root sockets

						printf("New listener connected...\n");
						// Resize the max value
						if (client_sd > max_sd) {
							max_sd = client_sd;
						}
					}
				} else if (idx == STDIN_FILENO) {
					// New message to listeners
					msg_sz = 0;
					if ((msg_sz = read(idx, msgBuf, MAXBUFFER)) > 0) {
						msgBuf[msg_sz - 1] = '\0';

						printf("Sending...\n");
						for (int idx_j = 0; idx_j <= max_sd; idx_j++) {
							if(FD_ISSET(idx_j, &root) && idx_j != STDIN_FILENO && idx_j != disp_id) {
								if (send(idx_j, msgBuf, msg_sz, 0) == -1) {
									printf("Error sending\n");
								}
							}
						}
					}
				} else {
					// Disconnected Listener
					msg_sz = 0;
					if ((msg_sz = recv(idx, msgBuf, MAXBUFFER, 0)) <= 0) {
						printf("Listener disconnected\n");
						close(idx);
						FD_CLR(idx, &root);
					} else {
						for (int idx_j = 0; idx_j <= max_sd; idx_j++) {
							if (FD_ISSET(idx_j, &root)) {
									if (idx_j != disp_id && idx_j != idx) {
										send(idx_j, msgBuf, msg_sz, 0);
									}
							}
						}
					}
				}
			}
		}
	}

	free(msgBuf);
	return 0;
}
