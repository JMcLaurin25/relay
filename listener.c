/*
 * Listener
 */

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
 
#include "handler.h"

#define MAXBUFFER 1024

// Saving terminal settings for restoration
struct termios saved_settings;
char *msgBuf;

// Referenced http://forums.fedoraforum.org/showthread.php?t=270335
static void reset_io(void);
static void set_input(void);

int main(void)
{
	set_input();

	int socket_id = 0, idx = 0;
	msgBuf = malloc(MAXBUFFER);
	if (!msgBuf) {
		printf("Failed to create message buffer\n");
		return 1;
	}

	memset(msgBuf, 0, MAXBUFFER);

	sig_init();

	// Create socket
	struct sockaddr_in list_addr;
	memset(&list_addr, 0, sizeof(list_addr));

	if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Failed to create socket\n");
		free(msgBuf);
		return 1;
	}

	list_addr.sin_family = AF_INET; // Set the socket family
	list_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	list_addr.sin_port = htons(2468); // Port to utilize

	// Connect to socket
	if (connect(socket_id, &list_addr, sizeof(list_addr)) < 0) {
		printf("Failed to connect to dispatcher\n");
		free(msgBuf);
		return 1;
	}

	// Reception loop
	while (1) {
		idx = recv(socket_id, msgBuf, MAXBUFFER, 0);
		if (idx == 0) {
			printf("Dispatcher is currently down\n");
			break;
		} else {
			msgBuf[idx] = '\0';
			printf(">%s\n", msgBuf);
			memset(msgBuf, '\0', MAXBUFFER);
		}
	}

	free(msgBuf);
	close(socket_id);
	return 0;
}

/*
 * Restores original settings
 */
static void reset_io(void)
{
	tcsetattr(STDIN_FILENO, TCSANOW, &saved_settings);
}

/*
 * Stores original settings, turns off stdin echo and restores on exit
 */
static void set_input(void)
{
	struct termios new_settings;

	// Check if terminal
	if (!isatty(STDIN_FILENO)) {
		printf("This is not a terminal\n");
		exit(1);
	}

	// Store Settings and target for reset
	tcgetattr(STDIN_FILENO, &saved_settings);
	atexit(reset_io);

	// Establish new settings
	tcgetattr(STDIN_FILENO, &new_settings);
	new_settings.c_lflag &= ~(ECHO); //Clears the echo
	new_settings.c_cc[VMIN] = 1;
	new_settings.c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_settings);
}

