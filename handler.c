#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "handler.h"

char *msgBuf;

/*
 * handler() function enables manipulation of struct vars elements
 */
void handler(int sig_num, siginfo_t *siginfo, void *context)
{
	(void) context;
	(void) siginfo;

	/*
	 * SIGINT	(2): Signal interrupt (Ctrl+C)
	 * SIGQUIT	(3): Quit Signal (Ctrl+D)
	 * SIGKILL	(9): MUST KILL
	 * SIGTERM	(15): Software termination
	 */

	// For all signals free the malloc'd memory
	switch (sig_num) {
		default:
			free(msgBuf);
			exit(0);
	}
}

/*
 * Sigaction initialization function to listen for program terminations
 */
void sig_init(void)
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));

	action.sa_sigaction = &handler;
	action.sa_flags = SA_SIGINFO;

	/*
	 * SIGINT	(2): Signal interrupt (Ctrl+C)
	 * SIGQUIT	(3): Quit Signal (Ctrl+D)
	 * SIGKILL	(9): MUST KILL
	 * SIGTERM	(15): Software termination
	 */

	sigaction(SIGINT, &action, NULL); 
	sigaction(SIGQUIT, &action, NULL);
	sigaction(SIGKILL, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
}
