Relay System

Write a pair of programs, a dispatcher and a listener, to act as a communication relay.

The dispatcher should accept text on its standard input, exiting when it receives an EOT character. It is acceptable to read standard input in buffered mode.

The listener should print out any text sent to the dispatcher. If the dispatcher exits (or is not running), the listener should exit normally.

It should be possible to run multiple instances of listener. All of them should print any text sent to the dispatcher.

If there are no running instances of listener when the dispatcher receives text, that text may be discarded.

Requirements

The name of the project should be relay.

The project should respond to make by building both programs. debug and profile targets should also be supported, building both applications with the appropriate compiler and linker flags.

Neither program should require arguments to run.

The dispatcher and listener programs may be run from different directories and even by different users, but will be run on the same machine.

The listener program should not echo any keyboard output to the screen other than what is being printed by the dispatcher. It should exit when Ctrl+C is sent to the program.

Challenge: Make dispatcher read unbuffered input from standard input. termios.h may be helpful for this challenge.
