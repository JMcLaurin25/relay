#ifndef HANDLER_C
#define HANDLER_C

// Made external to allow freeing of allocated memory
extern char *msgBuf; 

void handler(int sig_num, siginfo_t *siginfo, void *data);
void sig_init(void);

#endif
