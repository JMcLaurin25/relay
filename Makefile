
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

LDLIBS+=-lm

all: listener dispatcher

listener: listener.o handler.o

dispatcher: dispatcher.o handler.o

.PHONY: clean debug profile

clean:
	-rm *.o *.su listener dispatcher

debug: CFLAGS+=-g
debug: listener dispatcher

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: listener dispatcher
